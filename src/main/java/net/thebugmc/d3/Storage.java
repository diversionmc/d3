package net.thebugmc.d3;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface Storage {
    //
    // Values
    //
    
    /**
     * Check if a value exists given by this path.
     *
     * @return True if value is found, false otherwise.
     */
    default boolean exists(Object... path) {
        return findString(path).isPresent();
    }
    
    /**
     * Get all values in the level given by this path.
     *
     * @return Values map of this level (not deep).
     */
    Map<StorageView, StorageValue> values(Object... path);
    
    /**
     * Get all values in the level given by this path recursively.
     *
     * @return Values map of this level, including values of sublevels.
     */
    Map<StorageView, StorageValue> valuesDeep(Object... path);
    
    //
    // Levels
    //
    
    /**
     * Get a view by a path.
     *
     * @param path Path to by which to get a view.
     * @return Stored string.
     */
    default StorageView view(Object... path) {
        return new StorageView(this, path);
    }
    
    /**
     * Check if a level exists given by this path.
     *
     * @return True if level is found, false otherwise.
     */
    boolean existsLevel(Object... path);
    
    /**
     * Get all sublevel paths by a given path.
     *
     * @param path Path to look in.
     * @return List of paths.
     */
    List<StorageView> levels(Object... path);
    
    //
    // Value Setting
    //
    
    /**
     * Set a value in the storage.
     *
     * @param pathAndValue Path and a single value at the end.
     */
    Storage set(Object... pathAndValue);
    
    /**
     * Remove a single value given by a full path to it.
     *
     * @param path Path of a value.
     */
    Storage remove(Object... path);
    
    /**
     * Remove entire level given by a path.
     *
     * @param path Path of a level.
     */
    Storage removeLevel(Object... path);
    
    //
    // Main
    //
    
    /**
     * Get a storage value by a path.
     *
     * @param path Path to get value from.
     * @return Stored string.
     */
    Optional<StorageValue> find(Object... path);
    
    /**
     * Get a storage value list by a path.
     *
     * @param path Path to get value from.
     * @return Stored string split by newlines.
     */
    default List<StorageValue> list(Object... path) {
        return find(path).stream()
            .flatMap(s -> s.listString().stream())
            .map(StorageValue::new)
            .toList();
    }
    
    //
    // Find Primitives
    //
    
    /**
     * Get a string by a path.
     *
     * @param path Path to get value from.
     * @return Stored string.
     */
    default Optional<String> findString(Object... path) {
        return find(path).flatMap(StorageValue::findString);
    }
    
    /**
     * Find a boolean by a path.
     *
     * @param path Path to get value from.
     * @return Stored string converted to boolean. (should be true, case-insensitive)
     */
    default Optional<Boolean> findBoolean(Object... path) {
        return find(path).flatMap(StorageValue::findBoolean);
    }
    
    /**
     * Find a byte by a path.
     *
     * @param path Path to get value from.
     * @return Stored string converted to byte.
     */
    default Optional<Byte> findByte(Object... path) {
        return find(path).flatMap(StorageValue::findByte);
    }
    
    /**
     * Find a short by a path.
     *
     * @param path Path to get value from.
     * @return Stored string converted to short.
     */
    default Optional<Short> findShort(Object... path) {
        return find(path).flatMap(StorageValue::findShort);
    }
    
    /**
     * Find an int by a path.
     *
     * @param path Path to get value from.
     * @return Stored string converted to int.
     */
    default Optional<Integer> findInt(Object... path) {
        return find(path).flatMap(StorageValue::findInt);
    }
    
    /**
     * Find an float by a path.
     *
     * @param path Path to get value from.
     * @return Stored string converted to float.
     */
    default Optional<Float> findFloat(Object... path) {
        return find(path).flatMap(StorageValue::findFloat);
    }
    
    /**
     * Find an long by a path.
     *
     * @param path Path to get value from.
     * @return Stored string converted to long.
     */
    default Optional<Long> findLong(Object... path) {
        return find(path).flatMap(StorageValue::findLong);
    }
    
    /**
     * Find an double by a path.
     *
     * @param path Path to get value from.
     * @return Stored string converted to double.
     */
    default Optional<Double> findDouble(Object... path) {
        return find(path).flatMap(StorageValue::findDouble);
    }
    
    //
    // List Primitives
    //
    
    /**
     * Get a string list by a path.
     *
     * @param path Path to get value from.
     * @return Stored string split by newlines.
     */
    default List<String> listString(Object... path) {
        return find(path).stream()
            .flatMap(l -> l.listString().stream())
            .toList();
    }
    
    /**
     * Get a boolean list by a path.
     *
     * @param path Path to get value from.
     * @return Stored string split by newlines converted to boolean list. (should be true, case-insensitive)
     */
    default List<Boolean> listBoolean(Object... path) {
        return find(path).stream()
            .flatMap(l -> l.listBoolean().stream())
            .toList();
    }
    
    /**
     * Get a byte list by a path.
     *
     * @param path Path to get value from.
     * @return Stored string split by newlines converted to byte list.
     */
    default List<Byte> listByte(Object... path) {
        return find(path).stream()
            .flatMap(l -> l.listByte().stream())
            .toList();
    }
    
    /**
     * Get a short list by a path.
     *
     * @param path Path to get value from.
     * @return Stored string split by newlines converted to short list.
     */
    default List<Short> listShort(Object... path) {
        return find(path).stream()
            .flatMap(l -> l.listShort().stream())
            .toList();
    }
    
    /**
     * Get an int list by a path.
     *
     * @param path Path to get value from.
     * @return Stored string split by newlines converted to int list.
     */
    default List<Integer> listInt(Object... path) {
        return find(path).stream()
            .flatMap(l -> l.listInt().stream())
            .toList();
    }
    
    /**
     * Get an float list by a path.
     *
     * @param path Path to get value from.
     * @return Stored string split by newlines converted to float list.
     */
    default List<Float> listFloat(Object... path) {
        return find(path).stream()
            .flatMap(l -> l.listFloat().stream())
            .toList();
    }
    
    /**
     * Get an long list by a path.
     *
     * @param path Path to get value from.
     * @return Stored string split by newlines converted to long list.
     */
    default List<Long> listLong(Object... path) {
        return find(path).stream()
            .flatMap(l -> l.listLong().stream())
            .toList();
    }
    
    /**
     * Get an double list by a path.
     *
     * @param path Path to get value from.
     * @return Stored string split by newlines converted to double list.
     */
    default List<Double> listDouble(Object... path) {
        return find(path).stream()
            .flatMap(l -> l.listDouble().stream())
            .toList();
    }
}
