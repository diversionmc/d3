package net.thebugmc.d3.operation;

import net.thebugmc.parser.pattern.Sentence;
import net.thebugmc.parser.util.FilePointer;

public class ValueSentence extends Sentence {
    private final String s;

    public ValueSentence(FilePointer ptr, String s) {
        super(ptr);
        this.s = s;
    }

    public String toString() {
        return s;
    }
}