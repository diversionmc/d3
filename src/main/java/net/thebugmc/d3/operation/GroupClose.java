package net.thebugmc.d3.operation;

import net.thebugmc.parser.expression.CharPiece;
import net.thebugmc.parser.util.FilePointer;

public class GroupClose extends CharPiece {
    public GroupClose(FilePointer ptr) {
        super(ptr, ')');
    }
}
