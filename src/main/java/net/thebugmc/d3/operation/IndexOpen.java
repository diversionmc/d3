package net.thebugmc.d3.operation;

import net.thebugmc.parser.expression.CharPiece;
import net.thebugmc.parser.util.FilePointer;

public class IndexOpen extends CharPiece {
    public IndexOpen(FilePointer ptr) {
        super(ptr, '[');
    }
}
