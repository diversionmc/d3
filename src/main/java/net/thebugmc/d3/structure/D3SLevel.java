package net.thebugmc.d3.structure;

import net.thebugmc.d3.D3;
import net.thebugmc.parser.util.FilePointer;

import java.util.List;

import static java.util.Collections.singletonList;

public class D3SLevel extends D3Sentence {
    private final List<D3Sentence> items;
    
    public D3SLevel(FilePointer ptr, String path, List<D3Sentence> items) {
        super(ptr, path);
        this.items = items;
    }
    
    public List<D3SValue> values(D3 d3) {
        return path.endsWith("()")
            ? singletonList(new D3SMacro(pointer(), path.substring(0, path.length() - 2), items))
            : items.stream()
            .flatMap(i -> i.values(d3).stream())
            .map(v -> v.prependPath(path))
            .toList();
    }
}
