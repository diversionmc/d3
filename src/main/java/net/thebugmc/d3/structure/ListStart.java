package net.thebugmc.d3.structure;

import net.thebugmc.parser.expression.ExpressionPiece;
import net.thebugmc.parser.expression.PieceResult;
import net.thebugmc.parser.util.FilePointer;

import static net.thebugmc.parser.expression.PieceResult.LEAVE;

public class ListStart extends ExpressionPiece {
    String path;
    
    public ListStart(FilePointer ptr, String path) {
        super(ptr);
        this.path = path;
    }
    
    public PieceResult read(char c, FilePointer ptr) {
        return LEAVE;
    }
    
    public String path() {
        return path;
    }
}
