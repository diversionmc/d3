package net.thebugmc.d3.structure;

import net.thebugmc.d3.D3;
import net.thebugmc.parser.pattern.Sentence;
import net.thebugmc.parser.util.FilePointer;

import java.util.List;

public abstract class D3Sentence extends Sentence {
    protected String path;
    
    public D3Sentence(FilePointer ptr, String path) {
        super(ptr);
        this.path = path;
    }
    
    public abstract List<D3SValue> values(D3 d3);
    
    public String path() {
        return path;
    }
}
