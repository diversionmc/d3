package net.thebugmc.d3.structure;

import net.thebugmc.parser.expression.CharPiece;
import net.thebugmc.parser.util.FilePointer;

public class LevelEnd extends CharPiece {
    public LevelEnd(FilePointer ptr) {
        super(ptr, '<');
    }
}
