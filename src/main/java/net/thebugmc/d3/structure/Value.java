package net.thebugmc.d3.structure;

import net.thebugmc.parser.expression.CommentPiece;
import net.thebugmc.parser.expression.ExpressionPiece;
import net.thebugmc.parser.expression.PieceResult;
import net.thebugmc.parser.util.FilePointer;

import static net.thebugmc.parser.expression.PieceResult.*;

public class Value extends ExpressionPiece {
    boolean inList;
    boolean comment;
    char string = 0;
    char last;
    StringBuilder value = new StringBuilder();
    
    public Value(FilePointer ptr, boolean inList) {
        super(ptr);
        this.inList = inList;
    }
    
    public PieceResult read(char c, FilePointer ptr) {
        // Check value end
        if (c == '\n') {
            last = c;
            return inList && value().equals("]") ? REPLACE_LEAVE : TAKE;
        }
    
        if (string == 0) {
            if (c == '\'' || c == '\"') // Start ignoring comments
                string = c;
            else if (last == '/' && c == '/') { // Check comment
                value.deleteCharAt(value.length() - 1); // remove last
                comment = true;
                return REPLACE_TAKE;
            }
        } else if (c == string && last != '\\') // stop ignoring comments
            string = 0;
    
        // Add char to value
        value.append(last = c);
        return CONTINUE;
    }
    
    public ExpressionPiece replace(FilePointer ptr) {
        if (comment) return new CommentPiece(ptr, true, value.isEmpty() ? null : this);
        return new ListEnd(ptr);
    }
    
    public String value() {
        return value.toString().trim();
    }
}
