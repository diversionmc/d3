package net.thebugmc.d3.structure;

import net.thebugmc.d3.D3;
import net.thebugmc.parser.util.FilePointer;

import java.util.List;

import static java.util.Collections.singletonList;

public class D3SValue extends D3Sentence {
    private final String value;

    public D3SValue(FilePointer ptr, String path, String value) {
        super(ptr, path);
        this.value = value;
    }

    public static final String INLINE_MACRO_END = "()";

    public List<D3SValue> values(D3 d3) {
        return path.endsWith(INLINE_MACRO_END)
            ? singletonList(new D3SMacro(
            pointer(),
            path.substring(0, path.length() - INLINE_MACRO_END.length()),
            singletonList(new D3SValue(pointer(), "return", value))))
            : singletonList(this);
    }

    public String value() {
        return value;
    }

    public D3SValue prependPath(String path) {
        this.path = path + "." + this.path;
        return this;
    }
}