package net.thebugmc.d3;

import net.thebugmc.d3.operation.*;
import net.thebugmc.d3.structure.*;
import net.thebugmc.error.Result;
import net.thebugmc.error.TryR;
import net.thebugmc.error.TryS;
import net.thebugmc.error.Unit;
import net.thebugmc.parser.Parser;
import net.thebugmc.parser.expression.ExpressionPiece;
import net.thebugmc.parser.expression.NamePiece;
import net.thebugmc.parser.expression.NumberPiece;
import net.thebugmc.parser.expression.StringPiece;
import net.thebugmc.parser.pattern.ParsePattern;
import net.thebugmc.parser.pattern.PatternResult;
import net.thebugmc.parser.util.Pointable;

import java.io.*;
import java.util.ArrayList;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import static java.lang.Math.*;
import static java.lang.String.join;
import static java.util.Arrays.*;
import static java.util.Map.entry;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.IntStream.range;
import static java.util.stream.Stream.concat;
import static net.thebugmc.d3.D3.PrintStyle.COMPACT;
import static net.thebugmc.d3.StorageValue.splitList;
import static net.thebugmc.d3.operation.Operator.OperatorType.*;
import static net.thebugmc.error.Result.*;
import static net.thebugmc.parser.Parser.parser;
import static net.thebugmc.parser.expression.NumberPiece.tryBoolean;
import static net.thebugmc.parser.expression.NumberPiece.tryNumber;
import static net.thebugmc.parser.pattern.ParsePattern.match;
import static net.thebugmc.parser.pattern.ParsePattern.matchOne;
import static net.thebugmc.parser.util.ParserException.ASSERT;

/**
 * D3 - Third iteration of DNData
 *
 * @author Kirill Semyonkin - Diversion Network 2021
 */
public class D3 implements Storage {
    //
    // Boilerplate and parsing
    //

    private Optional<File> physical = empty();
    private long lastModified;

    private final Parser<D3Sentence> structureParser;
    private final Parser<ValueSentence> valueParser;
    private final AtomicReference<ParsePattern<ValueSentence>> valueRef = new AtomicReference<>();
    private final Function<List<ExpressionPiece>, String> levelPathParser = e -> {
        int pos = 0, limit = e.size();

        var path = new StringBuilder();
        while (pos < limit) {
            // Parse plain text with dots or numbers
            if (e.get(pos) instanceof NamePiece n) {
                if (!n.name().endsWith(".") && !path.isEmpty())
                    path.append('.');
                path.append(n.name());
                pos++;
                continue;
            }

            // Parse a value (except PATH, NUMBER, MACRO)
            var v = matchOne(e.subList(pos, limit), valueRef.get());
            if (v == null) return null;
            var r = v.result();
            if (r == null) return null;

            if (!(r + "").endsWith(".") && !path.isEmpty())
                path.append('.');
            path.append(r);
            pos += v.length();
        }

        return path + "";
    };
    private String currentLevel = ""; // parse and preprocess aid

    private final Map<String, UnaryOperator<String>> macros = new LinkedHashMap<>(Map.<String, UnaryOperator<String>>ofEntries(
        // Trigonometry
        entry("pi", s -> PI + ""),
        entry("sin", s -> sin(tryNumber(s)) + ""),
        entry("cos", s -> cos(tryNumber(s)) + ""),

        // Other math
        entry("sqrt", s -> sqrt(tryNumber(s)) + ""),
        entry("abs", s -> abs(tryNumber(s)) + ""),
        entry("floor", s -> floor(tryNumber(s)) + ""),
        entry("ceil", s -> ceil(tryNumber(s)) + ""),
        entry("round", s -> round(tryNumber(s)) + ""),

        // Operations
        entry("sum", s -> stream(splitList(s))
            .mapToDouble(NumberPiece::tryNumber)
            .sum() + ""),
        entry("mul", s -> stream(splitList(s))
            .mapToDouble(NumberPiece::tryNumber)
            .reduce((a, b) -> a * b)
            .orElse(0) + ""), // Mind that multiplying strings does not work like the operator
        entry("max", s -> stream(splitList(s))
            .mapToDouble(NumberPiece::tryNumber)
            .max()
            .orElse(0) + ""),
        entry("min", s -> stream(splitList(s))
            .mapToDouble(NumberPiece::tryNumber)
            .min()
            .orElse(0) + ""),

        // String manipulation
        entry("len", s -> s.length() + ""),
        entry("trim", s -> s.trim() + ""),
        entry("cut", s -> {
            // Try getting last argument as a number to cut to
            var toSeparator = s.lastIndexOf('\n');
            if (toSeparator < 0) throw new NumberFormatException();
            var to = tryNumber(s.substring(toSeparator + 1));
            if (to == null) throw new NumberFormatException();
            s = s.substring(0, toSeparator);

            // Try getting param before that to cut from
            var fromSeparator = s.lastIndexOf('\n');
            int fromInt = 0;
            tryFrom:
            {
                if (fromSeparator < 0) break tryFrom;
                var from = tryNumber(s.substring(fromSeparator + 1));
                if (from == null) break tryFrom;
                s = s.substring(0, fromSeparator);
                fromInt = from.intValue();
            }

            if (fromInt < 0) fromInt = s.length() + fromInt;
            if (to < 0) to = s.length() + to;

            return s.substring(fromInt, to.intValue());
        }),
        entry("from", s -> {
            // Try getting last argument as a number to start from
            var fromSeparator = s.lastIndexOf('\n');
            if (fromSeparator < 0) throw new NumberFormatException();
            var from = tryNumber(s.substring(fromSeparator + 1));
            if (from == null) throw new NumberFormatException();
            s = s.substring(0, fromSeparator);

            if (from < 0) from = s.length() + from;

            return s.substring(from.intValue());
        }),
        entry("index", s -> {
            // Try getting last argument as a substring to find
            var needleSeparator = s.lastIndexOf('\n');
            if (needleSeparator < 0) throw new NumberFormatException();
            var needle = s.substring(needleSeparator + 1);
            s = s.substring(0, needleSeparator);

            return s.indexOf(needle) + "";
        }),
        entry("lastIndex", s -> {
            // Try getting last argument as a substring to find
            var needleSeparator = s.lastIndexOf('\n');
            if (needleSeparator < 0) throw new NumberFormatException();
            var needle = s.substring(needleSeparator + 1);
            s = s.substring(0, needleSeparator);

            return s.lastIndexOf(needle) + "";
        }),
        entry("at", s -> {
            // Try getting last argument as an index number
            var indexSeparator = s.lastIndexOf('\n');
            if (indexSeparator < 0) throw new NumberFormatException();
            var index = tryNumber(s.substring(indexSeparator + 1));
            if (index == null) throw new NumberFormatException();
            s = s.substring(0, indexSeparator);

            if (index < 0) index = s.length() + index;

            return s.charAt(index.intValue()) + "";
        }),
        entry("repeat", s -> {
            // Try getting last argument as a repeat number
            var repeatSeparator = s.lastIndexOf('\n');
            if (repeatSeparator < 0) throw new NumberFormatException();
            var repeat = tryNumber(s.substring(repeatSeparator + 1));
            if (repeat == null) throw new NumberFormatException();
            s = s.substring(0, repeatSeparator);

            return s.repeat(repeat.intValue());
        }),
        entry("count", s -> {
            // Try getting last argument as a substring to find
            var needleSeparator = s.lastIndexOf('\n');
            if (needleSeparator < 0) throw new NumberFormatException();
            var needle = s.substring(needleSeparator + 1);
            s = s.substring(0, needleSeparator);

            // js .match(new RegExp("", "g")).length behaviour
            if (needle.isEmpty()) return s.length() + 1 + "";

            return (s.length() - s.replace(needle, "").length()) / needle.length() + "";
        }),
        entry("concat", s -> join("", splitList(s))),
        entry("n", s -> "\n"),

        // List processing
        entry("size", s -> splitList(s).length + ""),
        entry("distinct", s -> stream(splitList(s))
            .distinct()
            .collect(joining("\n"))),
        entry("filter", s -> {
            // Try getting last argument as macro name
            var macroSeparator = s.lastIndexOf('\n');
            if (macroSeparator < 0) throw new NumberFormatException();
            var macro = s.substring(macroSeparator + 1);
            s = s.substring(0, macroSeparator);

            var f = paths(macro).stream()
                .map(D3::pathToKey)
                .map(this::macro)
                .filter(Objects::nonNull)
                .findFirst()
                .orElseThrow();

            return stream(splitList(s))
                .filter(e -> tryBoolean(f.apply(e)))
                .collect(joining("\n"));
        }),
        entry("map", s -> {
            // Try getting last argument as macro name
            var macroSeparator = s.lastIndexOf('\n');
            if (macroSeparator < 0) throw new NumberFormatException();
            var macro = s.substring(macroSeparator + 1);
            s = s.substring(0, macroSeparator);

            var f = paths(macro).stream()
                .map(D3::pathToKey)
                .map(this::macro)
                .filter(Objects::nonNull)
                .findFirst()
                .orElseThrow();

            return stream(splitList(s))
                .map(f)
                .collect(joining("\n"));
        }),
        entry("reduce", s -> {
            // Try getting last argument as macro name
            var macroSeparator = s.lastIndexOf('\n');
            if (macroSeparator < 0) throw new NumberFormatException();
            var macro = s.substring(macroSeparator + 1);
            s = s.substring(0, macroSeparator);

            var f = paths(macro).stream()
                .map(D3::pathToKey)
                .map(this::macro)
                .filter(Objects::nonNull)
                .findFirst()
                .orElseThrow();

            return stream(splitList(s))
                .reduce((a, e) -> f.apply(a + "\n" + e))
                .orElse(null);
        }),
        entry("fold", s -> {
            // Try getting last argument as macro name
            var macroSeparator = s.lastIndexOf('\n');
            if (macroSeparator < 0) throw new NumberFormatException();
            var macro = s.substring(macroSeparator + 1);
            s = s.substring(0, macroSeparator);

            var f = paths(macro).stream()
                .map(D3::pathToKey)
                .map(this::macro)
                .filter(Objects::nonNull)
                .findFirst()
                .orElseThrow();

            // Try getting param before that as path for initial value
            var pathSeparator = s.lastIndexOf('\n');
            if (pathSeparator < 0) throw new NumberFormatException();
            var path = s.substring(pathSeparator + 1);
            s = s.substring(0, pathSeparator);

            var init = paths(path).stream()
                .map(this::findByExactPath)
                .flatMap(Optional::stream)
                .map(StorageValue::value)
                .findFirst().orElseThrow();

            return stream(splitList(s))
                .reduce(init, (a, e) -> f.apply(a + "\n" + e));
        }),
        entry("position", s -> {
            // Try getting last argument as macro name
            var macroSeparator = s.lastIndexOf('\n');
            if (macroSeparator < 0) throw new NumberFormatException();
            var macro = s.substring(macroSeparator + 1);
            s = s.substring(0, macroSeparator);

            var f = paths(macro).stream()
                .map(D3::pathToKey)
                .map(this::macro)
                .filter(Objects::nonNull)
                .findFirst()
                .orElseThrow();

            var args = splitList(s);
            return range(0, args.length)
                .filter(i -> tryBoolean(f.apply(args[i])))
                .findFirst()
                .orElse(-1) + "";
        }),
        entry("orElse", s -> {
            var args = splitList(s);
            if (args.length == 0) return "";
            if (args.length == 1) return args[0];
            if (args.length == 2)
                return args[args[0].isEmpty() ? 1 : 0];
            return args[tryBoolean(args[0]) ? 1 : 2];
        }),

        // D3 manipulation
        entry("currentLevel", s -> currentLevel),
        entry("levels", s -> {
            var cv = currentLevel;
            currentLevel = "";
            var path = join(".", splitList(cv + "\n" + s));
            var res = levels(path);
            currentLevel = cv;

            return res.stream()
                .map(StorageView::key)
                .map(k -> k.substring(path.isEmpty() ? 0 : path.length() + 1))
                .collect(joining("\n"));
        }),
        entry("keys", s -> {
            var cv = currentLevel;
            currentLevel = "";
            var path = join(".", splitList(cv + "\n" + s));
            var res = values(path);
            currentLevel = cv;

            return res.keySet().stream()
                .map(StorageView::key)
                .map(k -> k.substring(path.isEmpty() ? 0 : path.length() + 1))
                .collect(joining("\n"));
        }),
        entry("keysDeep", s -> {
            var cv = currentLevel;
            currentLevel = "";
            var path = join(".", splitList(cv + "\n" + s));
            var res = valuesDeep(path);
            currentLevel = cv;

            return res.keySet().stream()
                .map(StorageView::key)
                .map(k -> k.substring(path.isEmpty() ? 0 : path.length() + 1))
                .collect(joining("\n"));
        }),
        entry("values", s -> {
            var cv = currentLevel;
            currentLevel = "";
            var path = join(".", splitList(cv + "\n" + s));
            var res = values(path);
            currentLevel = cv;

            return res.entrySet().stream()
                .map(e -> e.getKey().key().substring(path.isEmpty() ? 0 : path.length() + 1)
                    + "\n" + (e.getValue() + "").replace("\\", "\\\\").replace("\n", "\\n"))
                .collect(joining("\n"));
        }),
        entry("valuesDeep", s -> {
            var cv = currentLevel;
            currentLevel = "";
            var path = join(".", splitList(cv + "\n" + s));
            var res = valuesDeep(path);
            currentLevel = cv;

            return res.entrySet().stream()
                .map(e -> e.getKey().key().substring(path.isEmpty() ? 0 : path.length() + 1)
                    + "\n" + (e.getValue() + "").replace("\\", "\\\\").replace("\n", "\\n"))
                .collect(joining("\n"));
        }),
        entry("exists", s -> exists(s) + ""),
        entry("existsLevel", s -> existsLevel(s) + "")
    ));

    /**
     * Create an empty default D3.
     */
    public D3() {
        structureParser = parser();
        valueParser = parser();

        // Structure parser
        var inList = new AtomicBoolean();
        var afterPath = new AtomicBoolean();
        structureParser
            .pre(() -> {
                inList.set(false);
                afterPath.set(false);
            })
            .piece((c, ptr) -> inList.get() || afterPath.get() ? new Value(ptr, inList.get()) : new Path(ptr))
            .pieceFinish(e -> {
                if (e instanceof ListStart) inList.set(true);
                if (e instanceof ListEnd) inList.set(false);
                afterPath.set(e instanceof Path);
            })
            .<LevelStart, LevelEnd>group(
                e -> e instanceof LevelStart,
                e -> e instanceof LevelEnd,
                (left, right, content) -> new LevelGroup(left.pointer(), content, left.path()))
            .<ListStart, ListEnd>group(
                e -> e instanceof ListStart,
                e -> e instanceof ListEnd,
                (left, right, content) -> new ListGroup(left.pointer(), content, left.path()))
            .pattern("level", e -> { // <level(...)>
                if (!(e.get(0) instanceof LevelGroup level)) return null;
                var res = match(level.content(), structureParser.patterns());
                return new PatternResult<>(1, new D3SLevel(level.pointer(), level.path(), res));
            })
            .pattern("list", e -> { // <list([value]...)]
                if (!(e.get(0) instanceof ListGroup list)) return null;
                e = list.content();

                var res = new StringBuilder();
                for (int i = 0, limit = e.size(); i < limit; i++) {
                    if (!(e.get(i) instanceof Value v)) return null;
                    if (i > 0) res.append('\n');
                    res.append(v.value());
                }

                return new PatternResult<>(1, new D3SValue(list.pointer(), list.path(), res.toString()));
            })
            .pattern("pair", e -> { // <path> <value>
                if (!(e.get(0) instanceof Path k)) return null;
                if (!(e.get(1) instanceof Value v)) return null;
                return new PatternResult<>(2, new D3SValue(k.pointer(), k.path(), v.value()));
            });

        // Value patterns
        @SuppressWarnings("LocalVariableNamingConvention")
        ParsePattern<ValueSentence> STRING = e -> { // <"string">
            if (!(e.get(0) instanceof StringPiece str)) return null;
            return new PatternResult<>(1, new ValueSentence(str.pointer(), str.content()));
        };
        @SuppressWarnings("LocalVariableNamingConvention")
        ParsePattern<ValueSentence> NUMBER = e -> { // <name> -> number (instead of failing lookup)
            if (!(e.get(0) instanceof NamePiece n)) return null;

            var num = tryNumber(n.name());
            if (num == null) return null;

            return new PatternResult<>(1, new ValueSentence(n.pointer(), n.name()));
        };
        @SuppressWarnings("LocalVariableNamingConvention")
        ParsePattern<ValueSentence> TUPLE = e -> { // <(...)>
            if (!(e.get(0) instanceof Tuple tuple)) return null;

            e = tuple.content();
            int pos = 0, limit = e.size();

            var res = new LinkedList<String>();
            while (pos < limit) {
                var v = valueParser.pattern("operation").parse(e.subList(pos, limit));
                if (v == null) return null;
                var r = v.result();
                if (r == null) return null;

                res.add(r + "");
                pos += v.length();

                if (pos == limit) break;
                if (!(e.get(pos++) instanceof Separator)) return null;
                if (pos == limit) res.add("");
            }

            return new PatternResult<>(1, new ValueSentence(tuple.pointer(), join("\n", res)));
        };
        @SuppressWarnings("LocalVariableNamingConvention")
        ParsePattern<ValueSentence> MACRO_BY_PATH = e -> { // <[name]><(...)>
            if (e.size() < 2) return null;
            if (!(e.get(0) instanceof Index index)) return null;
            if (!(e.get(1) instanceof Tuple tuple)) return null;

            e = index.content();
            var path = levelPathParser.apply(e);
            if (path == null) return null;

            // Find macro
            var macro = paths(path).stream()
                .map(D3::pathToKey)
                .map(this::macro)
                .filter(Objects::nonNull)
                .findFirst().orElse(null);
            if (macro == null) return null;

            e = tuple.content();
            int pos = 0, limit = e.size();

            var res = new LinkedList<String>();
            while (pos < limit) {
                var v = valueParser.pattern("operation").parse(e.subList(pos, limit));
                if (v == null) return null;
                var r = v.result();
                if (r == null) return null;

                res.add(r + "");
                pos += v.length();

                if (pos == limit) break;
                if (!(e.get(pos++) instanceof Separator)) return null;
            }

            return new PatternResult<>(2, new ValueSentence(index.pointer(), macro.apply(join("\n", res))));
        };
        @SuppressWarnings("LocalVariableNamingConvention")
        ParsePattern<ValueSentence> MACRO = e -> { // <name><(...)>
            if (e.size() < 2) return null;
            if (!(e.get(0) instanceof NamePiece n)) return null;
            if (!(e.get(1) instanceof Tuple g)) return null;

            // Find macro
            var macro = paths(n.name()).stream()
                .map(D3::pathToKey)
                .map(this::macro)
                .filter(Objects::nonNull)
                .findFirst().orElse(null);
            if (macro == null) return null;

            e = g.content();
            int pos = 0, limit = e.size();

            var res = new LinkedList<String>();
            while (pos < limit) {
                var v = valueParser.pattern("operation").parse(e.subList(pos, limit));
                if (v == null) return null;
                var r = v.result();
                if (r == null) return null;

                res.add(r + "");
                pos += v.length();

                if (pos == limit) break;
                if (!(e.get(pos++) instanceof Separator)) return null;
            }

            return new PatternResult<>(2, new ValueSentence(n.pointer(), macro.apply(join("\n", res))));
        };
        @SuppressWarnings("LocalVariableNamingConvention")
        ParsePattern<ValueSentence> PATH = e -> { // <a.b.c>
            if (!(e.get(0) instanceof NamePiece n)) return null;

            var v = paths(n.name()).stream()
                .map(this::findByExactPath)
                .flatMap(Optional::stream)
                .map(StorageValue::value)
                .findFirst().orElse(null);
            if (v == null) return null;

            return new PatternResult<>(1, new ValueSentence(n.pointer(), v));
        };
        @SuppressWarnings("LocalVariableNamingConvention")
        ParsePattern<ValueSentence> LEVEL = e -> { // [[a.(value).c]]
            if (!(e.get(0) instanceof Index index)) return null;

            e = index.content();
            var path = levelPathParser.apply(e);
            if (path == null) return null;

            var v = paths(path).stream()
                .map(this::findByExactPath)
                .flatMap(Optional::stream)
                .map(StorageValue::value)
                .findFirst().orElse(null);
            if (v == null) return null;

            return new PatternResult<>(1, new ValueSentence(index.pointer(), v));
        };
        @SuppressWarnings("LocalVariableNamingConvention")
        ParsePattern<ValueSentence> VALUE = e -> { // [!-+]<"string"|number|tuple|macro_by_path|level|macro|path>[[index[, index]]]
            int pos = 0, limit = e.size();

            // Get all unary operators before value
            var unary = new LinkedList<Operator>();
            while (e.get(pos) instanceof Operator op) {
                if (++pos == limit) return null; // unary must have something after
                if (op.type() != ADD     // unary must be ADD (ignored), SUB (turns into UMS), NOT
                    && op.type() != SUB
                    && op.type() != NOT) return null;
                if (op.type() == ADD) continue;
                if (op.type() == SUB) op.type(UMS);
                unary.push(op); // rtl
            }

            // Get value
            var value = matchOne(e.subList(pos, limit), STRING, NUMBER, TUPLE, MACRO_BY_PATH, LEVEL, MACRO, PATH);
            if (value == null) return null;
            pos += value.length();

            // Get index of a list
            var res = value.result();
            if (pos < limit && e.get(pos) instanceof Index i) {
                pos++;

                var list = asList(splitList(res + ""));

                e = i.content();
                var p = 0;
                limit = e.size();

                var v = valueParser.pattern("operation").parse(e.subList(p, limit));
                if (v == null) return null;
                var r = v.result();
                if (r == null) return null;

                var index = tryNumber(r + "");
                if (index == null) return null;
                var from = index.intValue();
                var start = from < 0 ? list.size() + from : from;

                p += v.length();
                var until = start + 1;

                tryParseUntil:
                if (p < limit && e.get(p) instanceof Separator) {
                    p++;

                    if (p == limit) {
                        until = list.size();
                        break tryParseUntil;
                    }

                    var v1 = valueParser.pattern("operation").parse(e.subList(p, limit));
                    if (v1 == null) return null;
                    var r1 = v1.result();
                    if (r1 == null) return null;

                    var to = tryNumber(r1 + "");
                    if (to == null) return null;
                    until = to.intValue();

                    p += v1.length();
                }

                if (p < limit) return null;

                var end = until < 0 ? list.size() + until : until;
                res = new ValueSentence(r.pointer(), join("\n", list.subList(start, end)));
            }

            // Wrap value in unary arithmetics
            for (var op : unary) res = new ValueSentence(op.pointer(), op.type().operate(res + "", null));

            return new PatternResult<>(pos, res);
        };
        valueRef.set(VALUE);

        // Value parser
        valueParser
            .piece((c, ptr) -> c == ',', (c, ptr) -> new Separator(ptr))
            .piece((c, ptr) -> c == '(', (c, ptr) -> new GroupOpen(ptr))
            .piece((c, ptr) -> c == ')', (c, ptr) -> new GroupClose(ptr))
            .piece((c, ptr) -> c == '[', (c, ptr) -> new IndexOpen(ptr))
            .piece((c, ptr) -> c == ']', (c, ptr) -> new IndexClose(ptr))
            .piece((c, ptr) -> c == '"' || c == '\'', (c, ptr) -> new StringPiece(ptr, c))
            .piece((c, ptr) -> Operator.check(c), (c, ptr) -> new Operator(ptr))
            .piece((c, ptr) -> NamePiece.check(c, "_$."), (c, ptr) -> new NamePiece(ptr, "_$."))
            .group(
                e -> e instanceof GroupOpen,
                e -> e instanceof GroupClose,
                (left, right, content) -> new Tuple(left.pointer(), content))
            .group(
                e -> e instanceof IndexOpen,
                e -> e instanceof IndexClose,
                (left, right, content) -> new Index(left.pointer(), content))
            .pattern("singlePrimitive", e -> {
                if (e.size() != 1) return null;
                return matchOne(e, STRING, NUMBER, TUPLE, LEVEL);
            })
            .pattern("operation", e -> { // <value> [<operator> <value>]...
                int pos = 0, limit = e.size();
                if (pos == limit) return null;

                // Parse <value> <operator> ... pairs
                var res = new LinkedList<Pointable>();
                while (pos < limit) {
                    // try value
                    var v = matchOne(e.subList(pos, limit), VALUE);
                    if (v == null) return null;
                    var r = v.result();
                    if (r == null) return null;

                    res.add(r);
                    pos += v.length();

                    if (pos < limit) {
                        if (e.get(pos) instanceof Separator) break;
                        if (!(e.get(pos++) instanceof Operator op)) return null;
                        res.add(op);
                    }
                }

                // Perform OPStrings in order
                Operator.solvers.forEach(s -> s.solve(res));

                ASSERT(res.size() == 1, () -> res.get(1).pointer(), "operation did not yield a singular result");
                return new PatternResult<>(pos, (ValueSentence) res.get(0));
            });
    }

    /**
     * Read a D3 from a file.
     *
     * @param textFile File to read.
     */
    public static Result<D3, IOException> from(File textFile) {
        var d3 = new D3();
        d3.physical = of(textFile);
        return d3
            .reload()
            .mapOk(ignored -> d3);
    }

    /**
     * Read a D3 from source code text.
     *
     * @param sourceCode Text to read.
     */
    public static Result<D3, IOException> from(String sourceCode) {
        var d3 = new D3();
        return tryRun(
            IOException.class,
            (TryR<IOException>) () -> d3.structureParser
                .text(sourceCode).build().stream()
                .flatMap(s -> s.values(d3).stream())
                .forEach(s -> d3.appendValue("", s)))
            .mapOk(ignored -> d3);
    }

    /**
     * Read a D3 from a stream.
     *
     * @param textStream Stream to read.
     */
    public static Result<D3, IOException> from(InputStream textStream) {
        var d3 = new D3();
        return tryRun(
            IOException.class,
            (TryR<IOException>) () -> d3.structureParser
                .readFrom(textStream).build().stream()
                .flatMap(s -> s.values(d3).stream())
                .forEach(s -> d3.appendValue("", s)))
            .mapOk(ignored -> d3);
    }

    private void appendValue(String pathTo, D3SValue v) {
        if (!pathTo.isEmpty()) pathTo += '.';

        if (v instanceof D3SMacro m) {
            appendMacro(pathTo, m);
            return;
        }

        var localPath = v.path();
        var destructure = false;
        if (localPath.length() >= 1 && localPath.charAt(localPath.length() - 1) == '<') {
            destructure = true;
            localPath = localPath.substring(0, localPath.length() - 1);
        }
        if (localPath.length() >= 2 && localPath.charAt(0) == '[' && localPath.charAt(localPath.length() - 1) == ']') {
            var e = valueParser
                .text(localPath.substring(1, localPath.length() - 1))
                .buildExpressionsGrouped();
            localPath = requireNonNull(levelPathParser.apply(e));
        }
        var path = pathTo + localPath;
        var value = v.value();

        var pos = -1;
        for (var i = 0; i < path.length(); i++)
            if (path.charAt(i) == '.') pos = i;

        var old = currentLevel;
        currentLevel = path.substring(0, max(0, pos));
        value = stream(splitList(value))
            .map(this::evaluate)
            .collect(joining("\n"));
        if (destructure) {
            var p = path.substring(pos + 1);
            removeLevel(p);

            var pairs = splitList(value);
            for (int i = 0; i < pairs.length - 1; i += 2) {
                var key = pairs[i];

                var kv = pairs[i + 1];
                var kv2 = new StringBuilder();
                for (int j = 0, lim = kv.length(); j < lim; j++) {
                    var c = kv.charAt(j);
                    if (c == '\\') {
                        if (++j == lim) throw new IllegalArgumentException();
                        var c2 = kv.charAt(j);
                        if (c2 == '\\') kv2.append('\\');
                        else if (c2 == 'n') kv2.append('\n');
                        else throw new IllegalArgumentException();
                    } else kv2.append(c);
                }

                set(p.isEmpty() ? key : p + "." + key, kv2);
            }
        } else set(path.substring(pos + 1), value);
        currentLevel = old;
    }

    private void appendMacro(String pathTo, D3SMacro m) {
        var path = pathTo + m.path();
        macro(path, input -> {
            var old = currentLevel;
            currentLevel += (currentLevel.isEmpty() ? "" : ".") + path + "()";

            set("input", input);
            m.items().stream()
                .flatMap(s1 -> s1.values(this).stream())
                .forEach(s1 -> appendValue(currentLevel, s1));

            var res = evaluate("return");
            removeLevel(); // currentLevel
            currentLevel = old;
            return res;
        });
    }

    //
    // Storage
    //

    private static String pathToKey(Object... path) {
        return stream(path)
            .map(p -> Objects.toString(p).replace(' ', '.'))
            .collect(joining("."));
    }

    private Object[] path(Object... path) {
        return path(currentLevel, path);
    }

    private static Object[] path(String currentLevel, Object... path) {
        return (currentLevel.isEmpty()
            ? stream(path)
            : concat(Stream.of(currentLevel), stream(path)))
            .flatMap(p -> stream(Objects.toString(p).split("[. ]")))
            .filter(p -> !p.isBlank())
            .toArray();
    }

    private List<Object[]> paths(Object... path) {
        var currentLevel = this.currentLevel;

        var res = new LinkedList<Object[]>();
        while (!currentLevel.isEmpty()) {
            res.add(path(currentLevel, path));
            currentLevel = currentLevel.substring(0, max(0, currentLevel.lastIndexOf('.')));
        }
        res.add(path(currentLevel, path));

        return res;
    }

    //
    // Values
    //

    private final Map<String, String> values = new LinkedHashMap<>();
    private Optional<D3> backup = empty();

    public Map<StorageView, StorageValue> values(Object... path) {
        var level = view(path);

        var res = new LinkedHashMap<StorageView, StorageValue>();
        values.entrySet().stream()
            .map(e -> entry(view(e.getKey()), new StorageValue(e.getValue())))
            .filter(e -> level.parentOf(e.getKey()) && e.getKey().path().length == level.path().length + 1)
            .forEach(e -> res.put(e.getKey(), e.getValue()));

        // Also add values of backup
        backup.ifPresent(b -> b
            .values(path)
            .forEach(res::putIfAbsent)); // backup should not override current file

        return res;
    }

    public Map<StorageView, StorageValue> valuesDeep(Object... path) {
        var level = view(path);

        var res = new LinkedHashMap<StorageView, StorageValue>();
        values.entrySet().stream()
            .map(e -> entry(view(e.getKey()), new StorageValue(e.getValue())))
            .filter(e -> level.parentOf(e.getKey()))
            .forEach(e -> res.put(e.getKey(), e.getValue()));

        // Also add values (deep) of backup
        backup.ifPresent(b -> b
            .valuesDeep(path)
            .forEach(res::putIfAbsent)); // backup should not override current file

        return res;
    }

    //
    // Levels
    //

    public StorageView view(Object... path) {
        return new StorageView(this, path(path));
    }

    public boolean existsLevel(Object... path) {
        var level = view(path);

        return values
            .keySet().stream()
            .anyMatch(key -> level.parentOf(view(key)))

            // Also check if level exists in parent
            || backup
            .map(b -> b.existsLevel(path))
            .orElse(false);
    }

    public List<StorageView> levels(Object... path) {
        var level = view(path);

        var res = new LinkedList<StorageView>();
        values.keySet().stream()
            .map(this::view)
            .filter(level::parentOf)
            .map(l -> level.view(l.path()[level.path().length]))
            .distinct()
            .forEach(res::add);

        // Also add levels of backup
        backup.ifPresent(backup -> backup
            .levels(path).stream()
            .filter(l -> !res.contains(l))
            .forEach(res::add));

        return res;
    }

    //
    // Value Setting
    //

    /**
     * Set a value in the storage.
     *
     * @param pathAndValue Path and a single value at the end.
     */
    public D3 set(Object... pathAndValue) {
        if (pathAndValue.length < 2)
            throw new IllegalArgumentException("path-value pair was not supplied");

        var value = pathAndValue[pathAndValue.length - 1];
        var path = copyOf(pathAndValue, pathAndValue.length - 1);

        if (value.getClass().isArray()
            && !value.getClass().componentType().isPrimitive())
            value = asList((Object[]) value);
        if (value instanceof Collection<?> c)
            value = c.stream()
                .map(Objects::toString)
                .collect(joining("\n"));

        synchronized (values) {
            values.put(pathToKey(path(path)), Objects.toString(value));
        }
        return this;
    }

    /**
     * Remove a single value given by a full path to it.
     *
     * @param path Path of a value.
     */
    public D3 remove(Object... path) {
        synchronized (values) {
            values.remove(pathToKey(path(path)));
        }
        return this;
    }

    /**
     * Remove entire level given by a path.
     *
     * @param path Path of a level.
     */
    public D3 removeLevel(Object... path) {
        var level = view(path);
        synchronized (values) {
            if (level.direct()) values.clear();
            else values.keySet()
                .removeIf(key -> key.startsWith(level.key() + "."));
        }
        return this;
    }

    //
    // Main
    //

    public Optional<StorageValue> find(Object... path) {
        return ofNullable(values.get(pathToKey(path(path))))
            .map(StorageValue::new)
            .or(() -> backup.flatMap(b -> b.find(path)));
    }

    private Optional<StorageValue> findByExactPath(Object... path) {
        return ofNullable(values.get(pathToKey(path)))
            .map(StorageValue::new)
            .or(() -> backup.flatMap(b -> b.findByExactPath(path)));
    }

    // Reorder

    private record Fold(String key, boolean level) {
    }

    private static Map<Fold, Object> fold(Map<String, String> values) {
        var res = new LinkedHashMap<Fold, Object>();
        values.forEach((k, v) -> {
            Map<Fold, Object> map = res;
            var path = k.split("\\.");
            for (int i = 0; i < path.length; i++) {
                var fold = new Fold(path[i], i + 1 < path.length);
                if (fold.level) //noinspection unchecked
                    map = (Map<Fold, Object>) map.computeIfAbsent(fold, f -> new LinkedHashMap<>());
                else map.put(fold, v);
            }
        });
        return res;
    }

    private static void unfold(Map<String, String> res, Map<Fold, Object> fold, String prefix) {
        fold.forEach((k, v) -> {
            if (k.level) //noinspection unchecked
                unfold(res, (Map<Fold, Object>) v, prefix + k.key + ".");
            else res.put(prefix + k.key, (String) v);
        });
    }

    /**
     * Reorder values in a way that joins levels together.
     * <p>
     * This will the insertion-order for levels inside other levels.
     * Consider following example (in D3):
     * <pre>
     * v
     *   a 1
     *   b 2
     *   c 3
     * &lt;
     * h 42
     * </pre>
     * In D3 Clear Print / in the Storage memory this example will look like so:
     * <pre>
     * v.a 1
     * v.b 2
     * v.c 3
     * h 42
     * </pre>
     * Setting <code>v.d = 4</code> will keep insertion order and place the value at the end of the Storage memory:
     * <pre>
     * v.a 1
     * v.b 2
     * v.c 3
     * h 42
     * v.d 4
     * </pre>
     * This gives following D3 file:
     * <pre>
     * v
     *   a 1
     *   b 2
     *   c 3
     * &lt;
     * h 42
     * v.d 4
     * </pre>
     * Often this result is unwanted, so reordering of the values in the memory is required to combine the v's together:
     * <pre>
     * v.a 1
     * v.b 2
     * v.c 3
     * v.d 4
     * h 42
     * </pre>
     * And now the D3 file will look like so:
     * <pre>
     * v
     *   a 1
     *   b 2
     *   c 3
     *   d 4
     * &lt;
     * h 42
     * </pre>
     */
    public synchronized D3 reorder() {
        var fold = fold(values);
        values.clear();
        unfold(values, fold, "");
        return this;
    }

    //
    // Backups
    //

    /**
     * Backup is the storage to look into if the value does not exist.
     * Any operations on this storage object does not modify the values of the backup this storage uses.
     *
     * @return Backup this storage is set to use.
     */
    public Optional<D3> findBackup() {
        return backup;
    }

    /**
     * Reset backup of this D3.
     */
    public D3 backupReset() {
        backup = empty();
        return this;
    }

    /**
     * Replace the backup chain of this D3.
     *
     * @param backup Backup to use.
     */
    public D3 backup(D3 backup) {
        this.backup = of(backup);
        return this;
    }

    /**
     * Get the last backup in this backup chain.
     *
     * @return Last backup in the chain.
     */
    public D3 backupTail() {
        if (backup.isEmpty()) return this;
        var backup = this.backup.get();
        while (backup.backup.isPresent())
            backup = backup.backup.get();
        return backup;
    }

    /**
     * Insert backup chain before the current backup chain.
     * <br>
     * Given <code>storage</code>, <code>chain1</code>, and <code>chain2</code>,
     * where <code>storage.chain1</code> is the current backup chain,
     * and the <code>chain2</code> is being inserted into <code>storage</code>,
     * the resulting chain will be <code>storage.chain2.chain1</code>.
     *
     * @param chain Backup chain to insert.
     */
    public D3 backupInsert(D3 chain) {
        chain.backupTail().backup = backup;
        backup = of(chain);
        return this;
    }

    /**
     * Append backup chain at the end of the current backup chain.
     * <br>
     * Given <code>storage</code>, <code>chain1</code>, and <code>chain2</code>,
     * where <code>storage.chain1</code> is the current backup chain,
     * and the <code>chain2</code> is being inserted into <code>storage</code>,
     * the resulting chain will be <code>storage.chain2.chain1</code>.
     *
     * @param chain Backup chain to append.
     */
    public D3 backupAppend(D3 chain) {
        backupTail().backup = of(chain);
        return this;
    }

    /**
     * Lossy insert backup chain before the current backup chain,
     * ignoring the rest of the chain of the given parameter.
     * <br>
     * Given <code>storage</code>, <code>backup</code>, <code>chain1</code>,and <code>chain2</code>,
     * where <code>storage.chain1</code> is the current backup chain,
     * and the <code>backup.chain2</code> is being inserted into <code>storage</code>,
     * the resulting chain will be <code>storage.backup.chain1</code>.
     *
     * @param backup Backup to insert.
     */
    public D3 backupLossy(D3 backup) {
        var previous = this.backup;
        this.backup = of(backup);
        previous.ifPresent(p -> backup.backup = of(p));
        return this;
    }

    //
    // Preprocess API
    //

    /**
     * Evaluate a value as D3 does during preprocessing.
     *
     * @param input Value to evaluate from.
     * @return Evaluated value, or input if any error occurred.
     */
    public String evaluate(String input) {
        return tryGet(
            Exception.class,
            (TryS<String, ?>)
                () -> simplifyNumber(valueParser
                    .text(input).build()
                    .get(0) + ""))
            .ok()
            .orElse(input);
    }

    public static String simplifyNumber(String number) {
        var list = splitList(number);
        for (int i = 0; i < list.length; i++) {
            var num = tryNumber(list[i]);
            if (num != null && list[i].endsWith(".0"))
                list[i] = num.longValue() + "";
        }
        return join("\n", list);
    }

    public static String simplifyNumber(double number) {
        return simplifyNumber(number + "");
    }

    /**
     * Get macro saved in this D3.
     *
     * @param macro Macro name.
     * @return Macro function.
     */
    public UnaryOperator<String> macro(String macro) {
        return macros.get(macro);
    }

    /**
     * Set a macro in this D3. Can override defaults.
     *
     * @param macro Macro name.
     * @param f     Macro function.
     */
    public D3 macro(String macro, UnaryOperator<String> f) {
        ASSERT(macro != null, "Macro name is null");
        ASSERT(f != null, "Macro " + macro + " is null");
        macros.put(macro, f);
        return this;
    }

    //
    // Output API
    //

    /**
     * Print styles for outputting the D3 file.
     *
     * @see #toString(PrintStyle)
     * @see #save(PrintStyle)
     */
    public enum PrintStyle {
        /**
         * Compact Style is a mix between {@link #TREE} and {@link #CLEAR} - it shortens paths when a level has only one value.
         */
        COMPACT {
            public void print(D3 d3, PrintWriter out) {
                parseCompact(d3, out, "", d3.valuesDeep()
                    .entrySet().stream()
                    .map(e -> entry(e.getKey().key(), e.getValue().value()))
                    .toList());
            }

            private void parseCompact(D3 d3, PrintWriter out, String spaces,
                                      List<Entry<String, String>> lines) {
                if (lines.isEmpty()) return;

                int pos = 0, limit = lines.size();
                var currentCommon = new LinkedList<String>();
                for (int i = 0; i < limit; i++) {
                    var path = asList(lines.get(i).getKey().split("\\."));
                    path = path.subList(0, path.size() - 1); // make sure "a" and "a.b" are not common

                    if (i == 0) {
                        currentCommon.addAll(path);
                        continue;
                    }

                    var newCommon = findCommon(path, currentCommon);
                    if (newCommon.isEmpty()) {
                        printLevel(d3, out, spaces, currentCommon, lines.subList(pos, i));
                        pos = i;
                        currentCommon.clear();
                        currentCommon.addAll(path);
                    } else {
                        currentCommon.clear();
                        currentCommon.addAll(newCommon);
                    }
                }
                printLevel(d3, out, spaces, currentCommon, lines.subList(pos, limit));
            }

            private void printLevel(D3 d3, PrintWriter out, String spaces,
                                    List<String> currentCommon,
                                    List<Entry<String, String>> lines) {
                if (lines.size() > 1) { // many values, print level
                    var level = join(".", currentCommon);
                    out.println(spaces + level);
                    parseCompact(d3, out, spaces + "  ", lines.stream()
                        .map(e -> entry(e.getKey().substring(level.length() + 1), e.getValue()))
                        .toList());
                    out.println(spaces + "<");
                } else printValue(d3, out, spaces, lines.get(0)); // single value, just output it
            }

            private <E> List<E> findCommon(List<E> a, List<E> b) {
                int pos = -1, limit = min(a.size(), b.size());
                var res = new ArrayList<E>(limit);
                while (++pos < limit) {
                    var v = a.get(pos);
                    if (!v.equals(b.get(pos))) return res;
                    res.add(v);
                }
                return res;
            }
        },
        /**
         * Tree Style is standard print style that uses only 1 path part per value/level, without using dots.
         */
        TREE {
            public void print(D3 d3, PrintWriter out) {
                parseTree(d3, out, "", d3.valuesDeep()
                    .entrySet().stream()
                    .map(e -> entry(e.getKey().key(), e.getValue().value()))
                    .toList());
            }

            private void parseTree(D3 d3, PrintWriter out, String spaces,
                                   List<Entry<String, String>> lines) {
                var entries = new LinkedList<Entry<String, String>>();
                String lastLevel = null;
                for (var line : lines) {
                    var key = line.getKey();
                    int pos = key.indexOf('.');
                    var level = key.substring(0, max(0, pos)); // empty = value, not level
                    if (lastLevel == null) lastLevel = level; // first entry
                    else if (!lastLevel.equals(level)) { // entry doesn't match last - current is next level
                        printLevel(d3, out, spaces, lastLevel, entries);
                        entries.clear();
                        lastLevel = level;
                    }
                    entries.add(entry(key.substring(pos + 1), line.getValue()));
                }
                if (!entries.isEmpty()) printLevel(d3, out, spaces, lastLevel, entries);
            }

            private void printLevel(D3 d3, PrintWriter out, String spaces, String level,
                                    List<Entry<String, String>> entries) {
                if (level.isEmpty()) // all values
                    entries.forEach(l -> printValue(d3, out, spaces, l));
                else {
                    out.println(spaces + level);
                    parseTree(d3, out, spaces + "  ", entries);
                    out.println(spaces + "<");
                }
            }
        },
        /**
         * Clearprint Style clearly shows full path before each value, with each value being own line (excl. lists), tree structure using &lt; is not printed.
         */
        CLEAR {
            public void print(D3 d3, PrintWriter out) {
                d3.valuesDeep()
                    .forEach((key, value) -> printValue(d3, out, "", entry(key.key(), value.value())));
            }
        };

        private static void printValue(D3 d3, PrintWriter out, String spaces,
                                       Entry<String, String> entry) {
            var key = entry.getKey();
            var value = entry.getValue();
            if (value.contains("\n")) {
                out.println(spaces + key + "[");
                for (var line : value.split("\n", -1))
                    out.println(spaces + "  " + escape(d3, line, true));
                out.println(spaces + "]");
            } else out.println(spaces + key + " " + escape(d3, value, false));
        }

        private static String escape(D3 d3, String s, boolean inList) {
            return s.isEmpty()
                || (inList && "]".equals(s))
                || !d3.evaluate(s).equals(s)
                || s.contains("//") || s.contains("/*")
                || s.startsWith(" ") || s.endsWith(" ")
                ? "\"" + StringPiece.escape(s) + "\"" : s;
        }

        /**
         * Serialize the D3 file into the given print writer.
         *
         * @param d3  D3 to print.
         * @param out Print output.
         */
        public abstract void print(D3 d3, PrintWriter out);
    }

    /**
     * Output this D3 using {@link PrintStyle#COMPACT COMPACT} print style.
     *
     * @return {@link #toString(PrintStyle) toString(COMPACT)}
     */
    public String toString() {
        return toString(COMPACT);
    }

    /**
     * Output this D3 using a given print style.
     *
     * @param style Style to print with.
     * @return Serialized D3.
     */
    public String toString(PrintStyle style) {
        var os = new ByteArrayOutputStream();
        style.print(this, new PrintWriter(os, true));
        return os.toString();
    }

    /**
     * Output this D3 to the file this D3 was created with.
     *
     * @return {@link #save(PrintStyle) save(COMPACT)}
     */
    public Result<Unit, IOException> save() {
        return save(COMPACT);
    }

    /**
     * Output this D3 to the file this D3 was created with using a given print style.
     *
     * @param style Style to save with.
     */
    public Result<Unit, IOException> save(PrintStyle style) {
        if (physical.isEmpty())
            return Error(new IOException("Cannot save a D3 made without a file"));
        var file = physical.get();

        var backup = this.backup;
        this.backup = empty();
        return tryRun(
            IOException.class,
            (TryR<IOException>) () -> {
                try (var f = new FileOutputStream(file); // TODO think about replacing try-with-resources
                     var ch = f.getChannel();
                     var ignored = ch.lock()) {
                    style.print(this, new PrintWriter(f, true));
                }
            })
            // Reattach backup after file save is complete
            .peek(s -> {
                lastModified = file.lastModified();
                this.backup = backup;
            });
    }

    /**
     * Reload this D3 from the file this D3 was created with.
     *
     * @throws IllegalArgumentException If no file was associated with this D3.
     */
    public Result<Unit, IOException> reload() {
        if (physical.isEmpty())
            return Error(new IOException("Cannot reload a D3 made without a file"));
        var file = physical.get();

        return tryRun(
            IOException.class,
            (TryR<IOException>) () -> {
                var res = structureParser
                    .readFrom(file).build().stream() // TODO readFrom -> Result and remove TryR
                    .flatMap(s -> s.values(this).stream());
                removeLevel(); // clear all
                res.forEach(s -> appendValue("", s));
            })
            .peek(s -> lastModified = file.lastModified());
    }

    /**
     * Reload this D3 from the file this D3 was created with if file's last modified date is newer than last load time.
     * If file does not exist or does not denote a file, does nothing.
     * Also reloads backup if it is D3. If backup is not D3, the rest of the backup chain is assumed to also not be D3.
     *
     * @throws IllegalArgumentException If no file was associated with this D3.
     */
    public Result<Boolean, IOException> reloadIfModified() {
        if (physical.isEmpty())
            return Error(new IOException("Cannot reload a D3 made without a file"));
        var file = physical.get();

        boolean reloaded = false;
        if (file.isFile()
            && file.lastModified() > lastModified) {
            var result = reload();
            if (result.isError()) return Error(result.error().get());

            reloaded = true;
        }

        var backupReload = findBackup()
            .map(D3::reloadIfModified);
        if (backupReload.isPresent()) {
            var result = backupReload.get();
            if (result.isError()) return Error(result.error().get());

            reloaded = reloaded || result.ok().get();
        }

        return Ok(reloaded);
    }
}