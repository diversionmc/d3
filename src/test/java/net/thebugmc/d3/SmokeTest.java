package net.thebugmc.d3;

import org.junit.jupiter.api.Test;

import static net.thebugmc.error.Functionals.some;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SmokeTest {
    @Test
    public void test() {
        var source = """
                     a 10
                     b
                       c 20
                     <
                     d.e 30
                     """;

        var d3 = D3.from(source).get();

        System.out.println(d3.toString(D3.PrintStyle.CLEAR));

        assertEquals(some("10"), d3.findString("a"));
        assertEquals(some("20"), d3.findString("b c"));
        assertEquals(some("30"), d3.findString("d", "e"));
    }
}
